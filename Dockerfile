FROM leanlabs/erlang:1.0.0

EXPOSE 80

COPY ./rel/wsserver /usr/local/leanlabs/wsserver/rel/wsserver
WORKDIR /usr/local/leanlabs/wsserver

CMD ["/usr/local/leanlabs/wsserver/rel/wsserver/bin/wsserver", "foreground"]

